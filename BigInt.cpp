#include "BigInt.h"
#include <iostream>

using namespace std;

BigInt::BigInt()
{
    this->digits = nullptr;
    this->size = 0;
}

BigInt::BigInt(const char *number)
{
    this->size = strlen(number);
    this->digits = new int[size];

    for (int i = 0; i < size; i++)
    {
        digits[i] = number[i] - '0';
    }
}

void BigInt::print()
{
    for (int i = 0; i < this->size; i++)
    {
        cout << this->digits[i];
    }

    cout << endl;
}

BigInt BigInt::add(const BigInt &other)
{
    int maxSize = (this->size > other.size) ? this->size : other.size;
    int *result = new int[maxSize + 1];

    for (int i = 0; i < maxSize; i++)
    {
        if (result[maxSize - i] == 1)
        {
            result[maxSize - i] += this->getNumber(this->size - 1 - i) + other.getNumber(other.size - 1 - i);
        }
        else
        {
            result[maxSize - i] = this->getNumber(this->size - 1 - i) + other.getNumber(other.size - 1 - i);
        }

        if (result[maxSize - i] > 9)
        {
            result[maxSize - i] -= 10;
            result[maxSize - 1 - i] = 1;
        }
    }

    BigInt sum;
    sum.digits = result;
    sum.size = maxSize + 1;

    return sum;
}

int BigInt::getNumber(int index) const
{
    return index >= 0 ? this->digits[index] : 0;
}
