#pragma once

#include <cstring>

using namespace std;

class BigInt
{
public:
    BigInt();
    BigInt(const char* number);
    void print();
    BigInt add(const BigInt& other);
    int size;
    int* digits;
private:
    int getNumber(int index) const;
};


